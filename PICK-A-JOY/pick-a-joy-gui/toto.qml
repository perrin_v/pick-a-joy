import QtQuick 2.0
import Qt.labs.templates 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.5
import QtGraphicalEffects.private 1.0
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

Item {
    id: startDialog
    width: 1024
    height: 768
    Rectangle {
        id: background
        color: "#ffffff"
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        Rectangle {
            id: bandeauRouge
            height: 100
            color: "#ff0000"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Rectangle {
                id: fondLogo
                x: 75
                y: 76
                width: 369
                height: 104
                color: "#f0f0f0"
            }
        }

        Rectangle {
            id: cadreSimoneGlobal
            x: 74
            y: 285
            width: 372
            height: 267
            color: "#ffffff"

            Rectangle {
                id: cadreSimoneInstructions
                height: 125
                color: "#d0d0d0"
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                TextArea {
                    id: textArea1
                    text: "presentation Simone"
                    anchors.leftMargin: 70
                    anchors.fill: parent
                }
            }

            Rectangle {
                id: cadreSimoneVerifications
                color: "#202020"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: cadreSimoneInstructions.bottom
                anchors.topMargin: 0

                Label {
                    id: label1
                    x: 74
                    y: 16
                    color: "#00ff00"
                    text: qsTr("Verfication de Simone")
                    styleColor: "#00ff00"
                }
            }
        }

        Rectangle {
            id: cadreIDGlobal
            x: 738
            y: 117
            width: 220
            height: 430
            color: "#00a000"

            Rectangle {
                id: cadreAttention
                y: 311
                height: 100
                color: "#f0f0f0"
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
            }

            Label {
                id: labelBienvenue
                x: 48
                y: 20
                width: 75
                height: 32
                text: "Bienvenue"
                minimumPixelSize: 21
                minimumPointSize: 21
                activeFocusOnTab: false
            }

            Rectangle {
                id: fondEntreeID
                x: 37
                y: 58
                width: 111
                height: 35
                color: "#c0c0c0"

                ComboBox {
                    id: listeID
                    anchors.fill: parent
                }
            }

            Rectangle {
                id: fondEntreePass
                x: 37
                y: 99
                width: 111
                height: 35
                color: "#c0c0c0"

                TextInput {
                    id: entreePass
                    text: qsTr("Text Input")
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }

            Rectangle {
                id: fondEntreeChiffrement
                x: 37
                y: 216
                width: 111
                height: 35
                color: "#c0c0c0"

                TextInput {
                    id: entreeChiffrement
                    text: qsTr("Text Input")
                    echoMode: TextInput.Normal
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }

            Rectangle {
                id: fondEntreeChiffrementRepet
                x: 37
                y: 257
                width: 111
                height: 35
                color: "#c0c0c0"

                TextInput {
                    id: entreeChiffrementRepet
                    text: qsTr("Text Input")
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }

            CheckBox {
                id: checkBoxSouvenir
                x: 8
                y: 140
                text: qsTr("Se souvenir de moi")
            }

            Button {
                id: boutonConnexion
                x: 174
                y: 104
                width: 25
                height: 25
                text: qsTr("Button")
            }

            Button {
                id: boutonChiffrement
                x: 174
                y: 262
                width: 25
                height: 25
                text: qsTr("Button")
            }

            Button {
                id: boutonOubli
                x: 174
                y: 171
                width: 25
                height: 25
                text: qsTr("Button")

                Label {
                    id: labelOubli
                    text: qsTr("Mot de passe oublie?")
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.left
                    anchors.rightMargin: 15
                }
            }
        }

        Rectangle {
            id: cadreInstructions
            x: 516
            y: 347
            width: 200
            height: 200
            color: "#0000a0"

            Label {
                id: label2
                text: qsTr("Label")
            }
        }
    }

}
