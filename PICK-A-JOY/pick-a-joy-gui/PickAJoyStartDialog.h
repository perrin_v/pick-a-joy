#ifndef PICKAJOYSTARTDIALOG_H
#define PICKAJOYSTARTDIALOG_H

#include <QQuickItem>
#include <QtWidgets/QDialog>

class StartDialog : public QDialog
{
    Q_OBJECT

public:
    /** Default constructor */
    StartDialog(QWidget *parent = 0);

    bool requestedNewCert();

protected:
    void closeEvent (QCloseEvent * event);

private slots:
    void loadPerson();

    /**
     * Warns the user that autologin is not secure
     */
    void notSecureWarning();

    void on_labelProfile_linkActivated(QString link);

private:
    /** Qt Designer generated object */
    QQuickItem *ui;

    bool reqNewCert;
};

#endif // PICKAJOYSTARTDIALOG_H
