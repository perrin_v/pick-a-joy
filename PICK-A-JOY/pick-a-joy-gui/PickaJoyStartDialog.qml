import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

ApplicationWindow
{
    visible: true
    width: 960
    height: 540
    title: qsTr("PICK-A-JOY")


    PickaJoyStartDialogForm
    {
        button1.onClicked: messageDialog.show(qsTr("Button 1 pressed"))
        state: Identified
    }

    MessageDialog
    {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption)
        {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}

