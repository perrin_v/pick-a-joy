import QtQuick 2.4
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

Item {
    id: item1
    width: 960
    height: 540

    property alias button1: button1

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        z: -1
        anchors.fill: parent

        Rectangle {
            id: rectangle2
            height: 80
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#b90606"
                }

                GradientStop {
                    position: 0.68
                    color: "#b90606"
                }

                GradientStop {
                    position: 1
                    color: "#4d0404"
                }

            }
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Rectangle {
                id: rectangle3
                x: 88
                y: 51
                width: 369
                height: 104
                color: "#a28686"
            }
        }

        Text {
            id: text1
            x: 630
            y: 86
            text: qsTr("Bienvenue")
            font.pixelSize: 48
        }

        Rectangle {
            id: rectangle4
            x: 492
            y: 170
            width: 442
            height: 200
            color: "#eeeded"

            Button {
                id: button1
                text: qsTr("Button")
                anchors.left: radioButton2.right
                anchors.leftMargin: 12
                anchors.verticalCenter: radioButton2.verticalCenter
            }

            RadioButton {
                id: radioButton1
                y: 48
                text: qsTr("")
                anchors.left: rectangle5.right
                anchors.leftMargin: 10
                anchors.verticalCenter: rectangle5.verticalCenter
            }

            RadioButton {
                id: radioButton2
                x: 218
                y: 98
                text: qsTr("")
                anchors.horizontalCenter: radioButton1.horizontalCenter
                anchors.verticalCenter: rectangle6.verticalCenter
            }

            Image {
                id: image1
                x: 65
                y: 54
                width: 15
                height: 15
                anchors.right: rectangle5.left
                anchors.rightMargin: 20
                anchors.verticalCenter: rectangle5.verticalCenter
                source: "qrc:/qtquickplugin/images/template_image.png"
            }

            Image {
                id: image2
                x: 65
                y: 100
                width: 15
                height: 15
                anchors.horizontalCenter: image1.horizontalCenter
                anchors.verticalCenter: rectangle6.verticalCenter
                source: "qrc:/qtquickplugin/images/template_image.png"
            }

            Rectangle {
                id: rectangle5
                x: 103
                y: 51
                width: 200
                height: 20
                color: "#ffffff"

                TextInput {
                    id: textInput1
                    text: qsTr("Text Input")
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }

            Rectangle {
                id: rectangle6
                x: 103
                y: 98
                width: 200
                height: 20
                color: "#ffffff"
                anchors.horizontalCenter: rectangle5.horizontalCenter

                TextInput {
                    id: textInput2
                    text: qsTr("Text Input")
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }
        }
    }
    states: [
        State {
            name: "Identified"
            
            PropertyChanges {
                target: rectangle4
                visible: false
            }
        }
    ]
}
