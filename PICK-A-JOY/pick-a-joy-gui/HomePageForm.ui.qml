import QtQuick 2.4

Item {
    width: 960
    height: 540

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        anchors.fill: parent

        Rectangle {
            id: rectangle2
            height: 100
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#d50f0f"
                }

                GradientStop {
                    position: 0.773
                    color: "#d50f0f"
                }

                GradientStop {
                    position: 1
                    color: "#3f0a0a"
                }

            }
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
        }

        Text {
            id: text1
            x: 289
            y: 184
            width: 399
            height: 103
            text: qsTr("fenetre principale")
            font.pixelSize: 50
        }
    }
}
