TEMPLATE = subdirs
#TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(pick-a-joy.pri)

SUBDIRS += \
    openpgpsdk \
    libbitdht \
    libretroshare \
    libresapi \
    pick-a-joy-gui

openpgpsdk.file = openpgpsdk/src/openpgpsdk.pro

libbitdht.file = libbitdht/src/libbitdht.pro

libretroshare.file = libretroshare/src/libretroshare.pro
libretroshare.depends = openpgpsdk libbitdht

libresapi.file = libresapi/src/libresapi.pro
libresapi.depends = libretroshare

plugins.file = plugins/plugins.pro
plugins.depends = retroshare_gui
plugins.target = plugins
