from django.conf.urls import patterns, include, url
from django.conf import settings
#from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

#urlpatterns = patterns(
#    url(r'^i18n/', include('django.conf.urls.i18n')),
#)

#urlpatterns += i18n_patterns(
#    url(r'^admin/', include(admin.site.urls)),
#    url(r'^$', 'homepage.views.home', name='home'),
#    url(r'^dumbledore$', 'homepage.views.newHome', name='newHome'),
#)

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'homepage.views.home', name='home'),
    url(r'^dumbledore$', 'homepage.views.newHome', name='newHome'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
