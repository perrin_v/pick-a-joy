#-*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class	Contact(models.Model):
    mail = models.CharField(max_length=30)

    def __unicode__(self):
        return u"%s" % self.mail