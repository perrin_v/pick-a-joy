# -*- coding: utf-8 -*-

from django.shortcuts import render
from homepage.forms import MailForm
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from homepage.models import Contact
from django.core.mail import send_mail

# Create your views here.

def home(request):
    if request.method == 'POST':
        form = MailForm(request.POST, auto_id=False)
        if form.is_valid():
            
            valide = True
            newContact = Contact()
            newContact.mail = form.cleaned_data['mail']
            newContact.save()
            
            message = u"Une nouvelle personne s'intéresse à pick-a-joy. Son adresse est: "
            message += newContact.mail
            send_mail('Nouvelle inscription pick-a-joy',
                      message,
                      'no-reply@pick-a-joy.com',
                      ['mladup.esevdla@gmail.com', 'pickajoy@gmail.com'], fail_silently=False)

            message = u"Bonjour, \nmerci de votre interêt pour PICK-A-JOY. Au fur et à mesure que le projet avance, nous vous tiendrons au courant via cette adresse mail. \nSi jamais vous ne voulez plus recevoir de nos nouvelles, écrivez-nous à pickajoy@gmail.com, et c'est les larmes aux yeux que nous effacerons votre adresse de notre liste de diffusion."
            send_mail('Bienvenue sur PICK-A-JOY',
                      message,
                      'no-reply@pick-a-joy.com',
                      [newContact.mail], fail_silently=False)

    else:
        form = MailForm(auto_id=False)

    return (render(request, "homePage.html", locals()))

def newHome(request):
    if request.method == 'POST':
        form = MailForm(request.POST, auto_id=False)
        if form.is_valid():
            #valide = True
            #newContact = Contact()
            #newContact.mail = form.cleaned_data['mail']
            #newContact.save()
            
            message = "Une nouvelle personne s'intéresse à pick-a-joy. Son adresse est: "
            message += newContact.mail
            send_mail('Nouvelle inscription pick-a-joy',
                      message,
                      'no-reply@pick-a-joy.com',
                      ['mladup.esevdla@gmail.com', 'pickajoy@gmail.com'], fail_silently=False)

            message = "Bonjour, \nmerci de votre interêt pour PICK-A-JOY. Au fur et à mesure que le projet avance, nous vous tiendrons au courant via cette adresse mail. \nSi jamais vous ne voulez plus recevoir de nos nouvelles, écrivez-nous à pickajoy@gmail.com, et c'est les larmes aux yeux que nous effacerons votre adresse de notre liste de diffusion."
            send_mail('Bienvenue sur PICK-A-JOY',
                      message,
                      'no-reply@pick-a-joy.com',
                      ['mladup.esevdla@gmail.com', 'pickajoy@gmail.com'], fail_silently=False)

    else:
        form = MailForm(auto_id=False)

    return (render(request, "newHomePage.html", locals()))

def profile(request):
    return (render(request, "profile.html", locals()))
