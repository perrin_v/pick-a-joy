#-*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    ID = forms.CharField(max_length=100, label=_(u"Identifiant libre"))
    password = forms.CharField(max_length=100, label=_(u"Mot de passe"))

class SubscribeForm(forms.Form):
    ID = forms.CharField(max_length=100, label=_(u"Identifiant libre"))
    password = forms.CharField(max_length=100, label=_(u"Mot de passe"))
    passwordConfirmation = forms.CharField(max_length=100, label=_(u"Entrer à nouveau votre mot de passe"))
    mail = forms.CharField(max_length=100, label=_(u"Adresse couriel"))


class MailForm(forms.Form):
    mail = forms.CharField(max_length=100, label="", widget=forms.TextInput(attrs={'placeholder': _(u"Votre adresse courrielle ?"), 'class': 'input', 'type' : 'email', 'id' : 'mailInput'}))
    #mail = forms.CharField(max_length=100, initial=_(u"Votre adresse courrielle ?"))