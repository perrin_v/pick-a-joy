"""
Django settings for phantom project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
from django.conf import global_settings

PROJECT_ROOT = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.insert(0, PROJECT_ROOT)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "templates"),
    )

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['lesailesbleues.alwaysdata.net',
                 'www.pick-a-joy.com',
                 'pick-a-joy.com']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'homepage'
)

MIDDLEWARE_CLASSES = (
#    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'phantom.urls'

WSGI_APPLICATION = 'phantom.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/


LOCALE_PATHS = (os.path.join(PROJECT_ROOT, "locale"),)

LANGUAGE_CODE = 'fr'
LANGUAGES = (
    ('en', 'English'),
    ('fr', 'Francais'),
    )

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + \
                              ('django.core.context_processors.i18n',)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = 'http://lesailesbleues.alwaysdata.net/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "public/static")
STATICFILES_DIRS = (os.path.join(PROJECT_ROOT, "static"),)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_HOST = 'smtp.alwaysdata.com'
EMAIL_PORT = 587


try:
    from local_settings import *
except ImportError as e:
    pass
