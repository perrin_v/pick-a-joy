#ifndef CONTACT_LIST_H
#define CONTACT_LIST_H

#include <glibmm/ustring.h>
#include <gtkmm.h>

class ContactList : public Gtk::Window
{
public:
  ContactList();
  virtual ~ContactList();

protected:
  Gtk::GtkListBox m_list;
};



#endif
