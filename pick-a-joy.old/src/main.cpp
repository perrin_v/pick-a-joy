#include <gtkmm.h>
#include "window.h"
#include "sqlite/sqlite3.h"

int main(int argc, char *argv[])
{
  Glib::RefPtr<Gtk::Application> app =
    Gtk::Application::create(argc, argv,
			     "org.gtkmm.examples.base");

  MainWindow window;
  window.set_default_size(800, 500);

  sqlite3 *database;
  sqlite3_open("pickajoy.db", &database);
  sqlite3_close(database);

  return app->run(window);
}
