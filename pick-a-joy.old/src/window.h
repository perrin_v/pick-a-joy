#ifndef WINDOW_H
#define WINDOW_H

#include <gtkmm.h>

class MainWindow : public Gtk::Window
{
public:
  MainWindow();
  virtual ~MainWindow();

protected:

  // Signal handlers:
  // Our new improved on_button_clicked(). (see below)
  void on_button_clicked(Glib::ustring data);

  // Child widgets:
  Gtk::Button m_Button_Send;
  Gtk::ListBox m_List_Box;
  Gtk::TextView m_Text_Entry, m_Text_Display;
  Gtk::Label m_Label1, m_Label2;
  Gtk::Paned m_Pane1, m_Pane2, m_Pane3, m_Pane4, m_Pane5;
};

#endif
