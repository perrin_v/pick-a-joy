#include <gtkmm.h>
#include "window.h"

MainWindow::MainWindow(): m_Button_Send("Envoi"),
			  m_Label1("contact number 1"),
			  m_Label2("contact number 2"),
			  m_Pane1(Gtk::ORIENTATION_HORIZONTAL),
			  m_Pane2(Gtk::ORIENTATION_VERTICAL),
			  m_Pane3(Gtk::ORIENTATION_HORIZONTAL)
{
  set_title("Pick-a-Joy");
  set_border_width(5);
  /*
  add(m_HBox);

  m_HBox.pack_start(m_VBox, Gtk::PACK_SHRINK);
  m_HBox.add(m_Button_Close);

  m_Frame_Multi.add(m_Label_Multi);
  m_VBox.pack_start(m_Frame_Multi, Gtk::PACK_SHRINK);
  */
  m_Text_Entry.set_editable(true);
  m_Text_Display.set_editable(false);
  add(m_Pane1);
  m_Pane1.pack1(m_List_Box);
  m_List_Box.add(m_Label1);
  m_List_Box.add(m_Label2);
  m_Pane1.pack2(m_Pane2);
  
  m_Pane2.pack1(m_Text_Display);
  m_Text_Display.get_buffer ()->set_text("the chat is displayed here");
  m_Pane2.pack2(m_Pane3);
  
  m_Pane3.pack1(m_Text_Entry);
  m_Text_Entry.get_buffer ()->set_text("messages are written here");
  m_Pane3.pack2(m_Button_Send);
  
  show_all_children();
}

MainWindow::~MainWindow()
{}
