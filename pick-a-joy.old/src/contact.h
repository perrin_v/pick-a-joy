#ifndef CONTACT_H
#define CONTACT_H
#include <glibmm/ustring.h>

using Glib::ustring;

class Contact
{
public:
  Contact();
  virtual ~Contact();

protected:
  ustring m_id;
  ustring m_name;
};



#endif
