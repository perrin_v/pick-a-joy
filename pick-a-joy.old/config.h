/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "pick-a-joy"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "mail@vperrin.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "Pick-a-Joy"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "Pick-a-Joy 1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "pick-a-joy"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1"

/* Version number of package */
#define VERSION "1"
