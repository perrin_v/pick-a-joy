# PRÉSENTATION #

Nous sommes traqués dans nos moindres faits et gestes, sollicités par la publicité tout le temps et asservit par la monnaie-dette. Il est temps de reprendre nos outils et nos vies en mains, collectivement.

Le projet PICK-A-JOY part d'une ambition concernant l'influence de la technologie sur les relations humaines. Nous cherchons à créer ce que les Internets auraient dû être : décentralisés, sécurisés, producteur de réel et promoteur de ce que nous avons de meilleur !

Dans cette optique nous avons imaginé Pick-a-joy, une plateforme généraliste de rencontres, non-sexiste ou chacun partage ce qu'il a envie de montrer, et rien de plus. Dans un monde ou les données personnelles de chacun sont vendues en masse à des entreprises de publicité, nous voulons offrir un moyen d'être connecté au monde sans devoir vendre son âme au préalable. C'est pourquoi les mots d'ordres de Pick-a-joy sont décentralisation, chiffrage, absence de publicité, neutralité du réseau et secret de la correspondance.

# ARCHITECTURE GLOBALE #

Pour atteindre ces objectifs, l'architecture globale du projet se présentera comme suit:

### un client local ###
Le coeur de Pick-a-Joy sera une application téléchargeable qui concentrera la plupart des fonctionnalités. Gestion du profil personnel (stockage et chiffrage de données), messagerie (instantanée + boite mail, également chiffrées), rédaction et partage d'articles avec la communauté. Toutes les interactions avec d'autres utilisateurs se feront en peer-to-peer. De plus, nous envisageons de baser les échanges réseau du client sur la technologie du navigateur TOR pour maximiser la sécurité.

### un serveur central ###
Un serveur central sera malgré tout nécessaire, mais nous comptons le faire le plus léger possible, qu'il ne serve qu'à combler les quelques fonctionnalités que nous ne parvenons pas à gérer en P2P pur. Ainsi, le serveur servira de standardiste (alias Simone) pour initier la première connection entre deux utilisateurs qui ne se connaissent pas. Simone pourra également servir de relai de messagerie si jamais les deux utilisateurs ne sont pas en ligne en même temps (la grande faiblesse du P2P). Toutefois, comme nous tenons à n'avoir aucune donnée utilisateur, tout message stocké sur le serveur sera effacé après un certain délai (système Poisson Rouge) ou quand le destinataire se connecte et le reçoit.


### Dispersions des données ###
Au-delà du client et du serveur, nous étudions la possibilité d'utiliser des technologies telles que Blockchain (utilisé par Bitcoin) et Storage .IO et TOX Ces technologies nous permettraient de garantir l’intégrité des échanges et l’absence “d’homme du milieu”, de fragmenter les données (profil utilisateur, articles etc.) sur de multiples machines, rendant ainsi PICK-A-JOY inattaquable sans détruire toutes les machines qui l’utilisent, et pourrait potentiellement réduire encore plus le rôle du serveur central.

# Le WIKI #
### dans le menu à gauche ###
Pour faire le tour plus profondément du projet, il est régulièrement mis à jour grâce à vos retours, n'hésitez pas à y revenir et à nous faire des suggestions également.

# LANGAGES UTILISÉS #

De manière générale, nous voulons éviter les langages et technologies propriétaires. De ce fait, le développement de Pick-a-Joy se fera en utilisant uniquement des langages Open Source et multi-plateformes. Il est prévu de développer le serveur en Python, avec le framework Django. Le client local quant à lui, risquant d'être relativement lourd en terme de calculs, ne peut être développé que dans un langage non-compilé. Il a donc été choisi de le développer en C++.

#Juridique#

Notre travail est déclaré sous licence creative commons CC - BY - NC

* Attribution : signature des auteurs initiaux (ce choix est obligatoire en droit français) (BY)
* Non Commercial : interdiction de tirer un profit commercial de l’œuvre sans autorisation des fondateurs Adrien Féraud et Vincent Perrin NC

Le partage, la modification et l'apport des tiers sont par contre encouragés, PICK-A-JOY est une œuvre collective.

# NOUS REJOINDRE #

Pick-A-Joy est un projet ambitieux et il n'en est actuellement qu'à ses débuts. La conception et le design sont prêts, nous avons les réponses à la plupart des problématiques technologiques. Il ne reste "plus que" le développement proprement dit. Mais l'équipe de PICK-A-JOY ne contient à l'heure actuelle que deux membres, dont un seul qui soit développeur de formation. Nous gardons donc une porte grande ouverte pour tout développeur qui voudrait venir nous prêter main forte. Si vous voulez nous rejoindre sur ce projet, n'hésitez pas à envoyer un mail à Adrien (lesailesbleues**AT**protonmail.com) et/ou Vincent (mladup.esevdla**AT**gmail.com)